from django.conf.urls import url
from hello import views

app_name = 'hello'
urlpatterns = [
  # The home view
  url(r'^$', views.home, name='home'),
  # Explicit home
  url(r'^home/$', views.home, name='home'),
  # Redirect to get token
  url(r'^gettoken/$', views.gettoken, name='gettoken'),
  # Mail view
  url(r'^mail/$', views.mail, name='mail'),
  # Events view 
  url(r'^events/$', views.events, name='events'),
  # Contacts view
  url(r'^contacts/$', views.contacts, name = 'contacts'),
]
