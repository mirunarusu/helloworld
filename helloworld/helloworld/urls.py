from django.conf.urls import url, include
from django.contrib import admin
from hello import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^hello/', include('hello.urls', namespace='hello')),
    url(r'^admin/', admin.site.urls),
]
